-- insert company --
INSERT INTO company ( id, company_name) VALUES ( 1, 'company_1');


-- insert branch --
INSERT INTO branch ( id, branch_name, company_id ) VALUES ( 1, 'branch_1',1);
INSERT INTO branch ( id, branch_name, company_id ) VALUES ( 2, 'branch_2',1);
INSERT INTO branch ( id, branch_name, company_id ) VALUES ( 3, 'branch_3',1);
