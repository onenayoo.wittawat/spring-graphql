package com.example.springgraphql.resources

import com.example.springgraphql.entity.Company
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository

@Repository
interface CompanyRepository : PagingAndSortingRepository<Company, Long>
