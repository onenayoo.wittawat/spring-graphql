package com.example.springgraphql.resources

import com.example.springgraphql.entity.Branch
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository

@Repository
interface BranchRepository : PagingAndSortingRepository<Branch, Long>
