package com.example.springgraphql.entity

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.ManyToOne
import javax.persistence.Table

@Entity
@Table
class Branch {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", insertable = true, updatable = true, unique = true, nullable = false)
    var id: Long? = null

    @Column(nullable = false)
    lateinit var branchName: String

    @ManyToOne
    lateinit var company: Company
}
