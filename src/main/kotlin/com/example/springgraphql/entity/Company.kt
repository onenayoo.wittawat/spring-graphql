package com.example.springgraphql.entity

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.OneToMany
import javax.persistence.Table

@Entity
@Table
class Company {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", insertable = true, updatable = true, unique = true, nullable = false)
    var id: Long? = null

    @Column(nullable = false)
    lateinit var companyName: String

    @OneToMany(mappedBy = "company", fetch = FetchType.EAGER)
    var branches: MutableList<Branch>? = null
}
