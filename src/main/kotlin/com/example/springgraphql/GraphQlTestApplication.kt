package com.example.springgraphql

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class GraphQlTestApplication

fun main(args: Array<String>) {
    runApplication<GraphQlTestApplication>(*args)
}
