package com.example.springgraphql.resolver

import com.example.springgraphql.resources.CompanyRepository
import graphql.kickstart.tools.GraphQLQueryResolver
import org.springframework.stereotype.Component

@Component
class QueryResolver(
    private val companyRepository: CompanyRepository,
) : GraphQLQueryResolver {

    fun companies() = companyRepository.findAll()
}
